Ext.define('SimpleLogin.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    fields: [
        'name', 'email', 'phone'
    ],

    data: { items: [
        { name: 'Charlie Guan', email: "CharlieGDev@gmail.com", phone: "250-858-8276" },
        { name: 'Darth Vader',     email: "electricArms@darkside.com",  phone: "778-933-1234" },
        { name: 'Obi Wan Kenobi',   email: "IveGotaBadFeeling@jedimaster.com",    phone: "604-318-3456" },
        { name: 'Mace Windu',     email: "ImSick&TiredOfSnakes@thisdamnplane.com",        phone: "902-458-8765" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
