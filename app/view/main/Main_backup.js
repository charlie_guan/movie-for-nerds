/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpleLogin.view.main.Main_backup', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main-backup',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'SimpleLogin.view.main.MainController',
        'SimpleLogin.view.main.MainModel',
        'SimpleLogin.view.main.List'
    ],

    controller: 'main',
    viewModel: 'main',
    // add this line to render this  main view to the browser viewport.
    plugins: 'viewport',
    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list',
        
        // add a Logout button at the header, so all tabs can see it.
        items: [{
        	xtype: 'button',
        	text: 'Logout',
        	margin: '10 0',
        	handler: 'onClickButton'
        }]
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'fa-home',
        // The following grid shares a store with the classic version's grid as well!
        items: [{
            xtype: 'mainlist'
        }]
    }, {
        title: 'Users',
        iconCls: 'fa-user',
        item: [{
        	xtype: 'mainlist'
        }]
        
        /* bind: {
                    html: '{loremIpsum}'
                } */
    }, {
        title: 'Groups',
        iconCls: 'fa-users',
        bind: {
            html: '{loremIpsum}'
        }
    }, {
        title: 'Settings',
        iconCls: 'fa-cog',
        bind: {
            html: '{loremIpsum}'
        }
    }]
});
