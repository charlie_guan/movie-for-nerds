/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpleLogin.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    
    // Add the onClickbutton function here to control how Logout Button behaves on Main.js.
    onClickButton: function() {
    	// removes the localStorage key/value pair for isLoggedIn.
    	localStorage.removeItem('isLoggedIn');
    	
    	// Since we logged out, we destroy the main view designed for logged in users.
    	this.getView().destroy();
    	
    	// After the main view is destroyed, display the login view again.
    	Ext.create({ xtype: 'login' });
    }     
});
