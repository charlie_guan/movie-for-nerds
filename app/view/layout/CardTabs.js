/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */

Ext.define('SimpleLogin.view.layout.CardTabs', {
    extend: 'Ext.tab.Panel',
    requires: [
        'Ext.layout.container.Card'
    ],
    xtype: 'layout-cardtabs',

    style: 'background-color:#dfe8f6; ',
    width: 1000,
    height: 800,

    defaults: {
        bodyPadding: 15
    },

    items:[
        {
            title: 'Tab 1',
            html:  "<h2>Hi!</h2>"
        },
        {
            title: 'Tab 2',
            html: 'This is tab 2 content.'
        },
        {
            title: 'Tab 3',
            html: 'This is tab 3 content.'
        }
    ]

});
