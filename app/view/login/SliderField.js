/**
 * Shows how the Slider control can be used in a form and participate like a form field.
 */
Ext.define('SimpleLogin.view.login.SliderField', {
    extend: 'Ext.form.Panel',
    
    requires: [
        'Ext.slider.Single'
    ],
    xtype: 'slider-field',
    
    width: 600,
    title: 'Display Settings',
    bodyPadding: 10,
   
    initComponent: function(){
        this.msgTpl = new Ext.Template(
            'Brightness: <b>{brightness}%</b><br />',
            'Contrast: <b>{contrast}%</b><br />',
            'Sharpness: <b>{sharpness}%</b>'
        );
        Ext.apply(this, {
            defaults: {
                labelWidth: 125,
                anchor: '95%',
                tipText: function(thumb){
                    return String(thumb.value) + '%';
                } 
            },
            defaultType: 'slider',
            items: [{
                fieldLabel: 'Brightness',
                value: 50,
                name: 'brightness'
            },{
                fieldLabel: 'Contrast',
                value: 80,
                name: 'contrast'
            },{
                fieldLabel: 'Sharpness',
                value: 25,
                name: 'sharpness'
            }],
            bbar: [{
                text: 'Max All',
                scope: this,
                handler: this.onMaxAllClick
            }, '->', {
                text: 'Save',
                scope: this,
                handler: this.onSaveClick
            }, {
                text: 'Reset',
                scope: this,
                handler: this.onResetClick
            }]
        });
        this.callParent();
    },
    
    onMaxAllClick: function(){
        Ext.suspendLayouts();
        this.items.each(function(c){
            c.setValue(100);
        });
        Ext.resumeLayouts(true);
    },
    
    onSaveClick: function(){
        Ext.Msg.alert({
            title: 'Settings Saved',
            msg: this.msgTpl.apply(this.getForm().getValues()),
            icon: Ext.Msg.INFO,
            buttons: Ext.Msg.OK
        }); 
    },
    
    onResetClick: function(){
        this.getForm().reset();
    }
});
