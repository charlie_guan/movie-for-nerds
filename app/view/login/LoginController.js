/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
Ext.define('SimpleLogin.view.login.LoginController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.login',
	
	// *hmm...
	onLoginClick: function() {
		// This function is called when Login button is pressed.
		// That behaviour was defined in Login.js.
	
		// Normally, verify user's credentials on server side lookup;
		// however we're not doing it here. If we were to do this,
		// we usually would use Ajex or Rest.
		
		// We have successfully logged in!
		// ...kinda. We only modified (read: hack) local boolean value.
		
		// https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
		// I guess using localStorage is an easy way to make some variable 
		// visible to every js file in the whole program. Kinda like global variables,
		// but not only global to the current file, but for everywhere in the program.
		localStorage.setItem("isLoggedIn", true);
		
		// Move pass the login window.
		this.getView().destroy();
		
		// Since the user has *successfully* logged in, show them the
		// main view in the viewport.
		// Instantiate the /app/view/main/Main.js
		Ext.create({ xtype: 'app-main' });
	}
});
