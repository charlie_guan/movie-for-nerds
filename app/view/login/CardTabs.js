/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
Ext.define('SimpleLogin.view.layout.CardTabs', {
    extend: 'Ext.tab.Panel',
    requires: [
        'Ext.layout.container.Card'
    ],
    xtype: 'layout-cardtabs',

    style: 'background-color:#dfe8f6; ',
    width: 800,
    height: 600,

    defaults: {
        bodyPadding: 15
    },

    items:[
        {
            title: 'Star Wars The Force Awaken',
            html:   
            "<img src='app/store/starwars.jpg' alt='starwars vii' width='800' height='500'>"
        },
        {
            title: 'Deadpool',
            html: "<img src='app/store/deadpool.jpg' alt='deadpool' width='800' height='500'>"
        },
        {
            title: 'Batman vs Superman: Dawn of Justice',
            html: "<img src='app/store/batman_vs_superman.jpg' alt='Batman vs Superman' width='800' height='500'>"
        },
        {
        	title: 'Warcraft',
        	html: "<img src='app/store/warcraft.jpg' alt='Warcraft' width='800' height='500'>"
        }
    ]

});
