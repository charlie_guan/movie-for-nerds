/* Greetings! The following code is created/modifed by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
Ext.define('SimpleLogin.view.login.Login', {
	extend: 'Ext.window.Window',
	xtype: 'login',
	
	requires: [
		'SimpleLogin.view.login.LoginController',
		'Ext.form.Panel'
	],
	
	controller: 'login',
	bodyPadding: 10,
	title: 'Movies for Nerds!',
	closable: false,
	autoShow: true,
	
	items: {
		xtype: 'form',
		reference: 'form',
		items: [{
			xtype: 'displayfield',
			hideEmptyLabel: false,
			value: 'Hi! Login to see the best upcoming movies for nerds!'
			},
		
		{
			xtype: 'textfield',
			name: 'username',
			fieldLabel: 'Username',
			allowBlank: false
		}, {
			xtype: 'textfield',
			name: 'password',
			inputType: 'password',
			fieldLabel: 'Password',
			allowBlank: false
		}, {
			xtype: 'displayfield',
			hideEmptyLabel: false,
			value: 'Any non-blank password will do.<br>I\'m serious. We\'re trusting like that.'
		}],
		/* the following is a bit confusing: it says "buttons", but what it actually
		 * means, is to create a bottom dock in the panel, and add buttons to the
		 * dock. This is synonym for the fbar config.
		 * http://docs.sencha.com/extjs/6.0/6.0.0-classic/#!/api/Ext.form.Panel-cfg-buttons
		 */	
		buttons: [{
			text: 'Login',
			formBind: true,
			listeners: {
				click: 'onLoginClick'
			}
		}]
	}
});
			


