/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('SimpleLogin.Application', {
    extend: 'Ext.app.Application',
    
    name: 'SimpleLogin',

    stores: [
        // TODO: add global / shared stores here
        // Maybe we can put isLoggedIn here?
    ],
    
    // Add views here:
    // Those dots tracks the actual file path.
    // In original doc, this was "requires", instead of view in explanation.
    views: [
    	'SimpleLogin.view.login.Login',
    	'SimpleLogin.view.main.Main'
    ],
    
    
    launch: function () {
    	/* launch method is the function executed when the application has 
    	 * finished loading everything. Ideal place to check if user is logged in.
    	 */
    	
        // According to the doc, this type of application (which type? classic?) could 
        // use any type of storage, such as Cookies, LocalStorage, etc.
        // We're using LocalStorage here, and we're defining the "isLoggedIn" used in
        // LocalStorage back in LoginController.js.
        
        // BTW JavaScript seems to prefer capitalize the first letter of a js filename?
        
        var loggedIn;
        // loggedIn is a boolean variable, storing login status.
        loggedIn = localStorage.getItem("isLoggedIn");
        
        // If loggedIn is true, display main view.
        // Otherwise, display login view.
        // Original documentation uses a ternary operator like
        // xtype: loggedIn ? 'app-main' : 'login'
        // But I don't like it.
        
        /*
        Ext.create({
        	if (loggedIn) 
        		xtype: 'app-main',
        	else 
        		xtype: 'login',
        	
        });
        */
        
        // Interestingly, you can't write like that (as in the doc) if you don't use ternary operater.
        // Have to write it like this:
        
        if (loggedIn == true) {
        	Ext.create({ xtype: 'app-main' });
        } else {
        	Ext.create({ xtype: 'login'    });
        }
    },

	// Provided in the original file. No change here.
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
