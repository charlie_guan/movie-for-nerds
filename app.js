/* Greetings! The following code is created/modified by: 
 * Charlie Guan
 * charliegdev@gmail.com
 * http://charlieportal.com
 * Following Sencha Documentations at:
 * http://docs.sencha.com/
 */
/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'SimpleLogin',

    extend: 'SimpleLogin.Application',

    requires: [
        'SimpleLogin.view.main.Main'
    ]
    
    // must delete the previous trailing comma after requires.

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    // ********************************************
    // Remove the mainView config, so that we can perform evaluations in 
    // Ext.application's lauch method.
    // If we would have kept this, no login window would be shown. We'll
    // be taken straight to main view.
//    mainView: 'SimpleLogin.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to SimpleLogin.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
